//
//  PreviewQuestionView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 31.08.2022.
//

import SwiftUI

struct PreviewQuestionView: View {
//    @EnvironmentObject var subscriptionData: SubscriptionsViewModel
    @EnvironmentObject var gameData: GameViewModel
    
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @Binding var previewQuestionView: Bool
    
    @State var indexPreviewQuestion: Int = 0
    
    var body: some View {
        
        let widthIconMicrofon = UIScreen.main.bounds.width / 2.09
        let widthButton = UIScreen.main.bounds.width - (16 * 2)
        let heightButton = widthButton / 5.27
        
        VStack(spacing: 0){
            HeaderRuleView(showHelpView: $previewQuestionView)
            
            Text(gameData.getNameBySet(set: gameData.currentSet))
                .fontWeight(.heavy)
                .font(.largeTitle)
                .foregroundColor(Color.init(hex: "#56456C"))
            
            Image(gameData.getImageCircleBySet(set: gameData.currentSet))
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: widthIconMicrofon)
                .padding(.top, 25)
            
            VStack(alignment: .leading, spacing: 10){
                Divider()
                    .background(Color.white.opacity(0.2))
                
                if !gameData.previewTasks.isEmpty{
                    TabView(selection: $indexPreviewQuestion){
                        ForEach(gameData.previewTasks.indices, id: \.self){i in
                            HStack{
                                Text(gameData.previewTasks[i])
                                    .fontWeight(.semibold)
                                    .font(.title3)
                                
                                Spacer(minLength: 0)
                            }
                            .padding(.horizontal, 16)
                            .id(i)
                        }
                    }
                    .tabViewStyle(.page(indexDisplayMode: .never))
                    .frame(height: 100)
                    
                    VStack{
                        
                        HStack{
                            ForEach(0..<3, id: \.self){i in
                                if indexPreviewQuestion == i{
                                    LinearGradient(colors: [Color.init(hex: "DB5A3D"), Color.init(hex: "DB5A3D")], startPoint: .leading, endPoint: .trailing)
                                        .clipShape(Circle())
                                        .frame(width: 10, height: 10)
                                }else{
                                    Circle()
                                        .foregroundColor(Color.init(hex: "D9D9D9"))
                                        .frame(width: 10, height: 10)
                                }
                            }
                        }
                        
                        HStack{
                            Text("\(gameData.previewIndexTasks[indexPreviewQuestion])/\(gameData.getCountQuestions(type: gameData.currentSet))")
                                .fontWeight(.semibold)
                                .font(.callout)
                                .foregroundColor(Color.init(hex: "988CA7"))
                        }
                        .frame(maxWidth: .infinity, alignment: .center)
                    }
                }
                
                Button {
                    withAnimation {
                        previewQuestionView.toggle()
                    }
//                    subscriptionData.openPremium.toggle()
                } label: {
                    ZStack{
                        LinearGradient(colors: [Color.init(hex: "FA6C30"), Color.init(hex: "FF9900")], startPoint: .leading, endPoint: .trailing)
                        
                        Text("Try!".localized(languages))
                            .fontWeight(.black)
                            .font(.title3)
                            .foregroundColor(.white)
                    }
                    .frame(width: widthButton, height: heightButton)
                    .cornerRadius(18)
                    .padding(.top, 25)
                }
                .buttonStyle(.plain)
                
            }
            .padding(.horizontal, 16)
            .padding(.top, 35)
        }
        .onChange(of: previewQuestionView) { _ in
            indexPreviewQuestion = 0
        }
    }
}

struct PreviewQuestionView_Previews: PreviewProvider {
    static var previews: some View {
        PreviewQuestionView(previewQuestionView: .constant(true))
            .environmentObject(GameViewModel())
    }
}
