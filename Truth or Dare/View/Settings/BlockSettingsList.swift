//
//  BlockSettingsList.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 28.08.2022.
//

import SwiftUI

struct BlockSettingsList: View{
    @EnvironmentObject var subscriptionsData: SubscriptionViewModel
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @Environment(\.openURL) var openURL
    @State var openSelectedLanguage: Bool = false
    var body: some View{
        ZStack{
            
            NavigationLink(isActive: $subscriptionsData.openPremium, destination: {
                PremiumView()
            }, label: {
                EmptyView()
            })
            
            NavigationLink(isActive: $openSelectedLanguage) {
                SelectionLanguage()
            } label: {
                EmptyView()
            }
            
            
            VStack(spacing: 0){
                Button(action: {
                    openSelectedLanguage.toggle()
                }, label: {
                    ItemList(image: getFlagByLanguage(), title: getCurrentLanguage())
                })
                .buttonStyle(.plain)
                .padding(.bottom, 25)
                
                VStack(spacing: 2){
                    Button {
                        subscriptionsData.openPremium = true
                    } label: {
                        ItemList(image: "ImageDiamond", title: "Buy full access".localized(languages), type: .CropUP)
                    }
                    .buttonStyle(.plain)
                    
                    Button {
                        subscriptionsData.restoreProduct()
                    } label: {
                        ItemList(image: "ImageShoppingCart", title: "Restore Purchases".localized(languages), type: .CropUPAndBottom)
                    }
                    .buttonStyle(.plain)
                    
                    Button {
                        openURL(URL(string: "https://google.com")!)
                    } label: {
                        ItemList(image: "ImageComment", title: "Write a feedback".localized(languages), type: .CropUPAndBottom)
                    }
                    .buttonStyle(.plain)
                    
                    
                    Button {
                        openURL(URL(string: "https://google.com")!)
                    } label: {
                        ItemList(image: "ImageStarHalf", title: "Rate the app".localized(languages), type: .CropUPAndBottom)
                    }
                    .buttonStyle(.plain)
                    
                    
                    Button {
                        actionSheetShare(url: "https://google.com")
                    } label: {
                        ItemList(image: "ImageShareFill", title: "Share".localized(languages), type: .CropBottom)
                    }
                    .buttonStyle(.plain)
                }
            }
        }
    }
    
    func actionSheetShare(url: String) {
        guard let data = URL(string: url) else { return }
        let av = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true, completion: nil)
    }
    
    func getCurrentLanguage() -> String{
        switch languages {
        case .english:
            return "English".localized(languages)
        case .russian:
            return "Russian".localized(languages)
        case .spanish:
            return "Spanish".localized(languages)
        case .german:
            return "German".localized(languages)
        case .french:
            return "French".localized(languages)
        }
    }
    
    func getFlagByLanguage() -> String{
        switch languages {
        case .english:
            return "ImageFlagEn"
        case .russian:
            return "ImageFlagRu"
        case .french:
            return "ImageFlagFr"
        case .german:
            return "ImageFlagGe"
        case .spanish:
            return "ImageFlagSp"
        }
    }
}

struct BlockSettingsList_Previews: PreviewProvider {
    static var previews: some View {
        BlockSettingsList()
    }
}
