//
//  CustomHeaderSettingsView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI

struct CustomHeaderSettingsView: View{
    @Environment(\.presentationMode) var presentationMode
    var body: some View{
        HStack{
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                CustomButtonBack()
            }
            .buttonStyle(.plain)
            .padding(.leading, 16)
            
            Spacer(minLength: 0)
        }
    }
}

struct CustomHeaderSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderSettingsView()
    }
}
