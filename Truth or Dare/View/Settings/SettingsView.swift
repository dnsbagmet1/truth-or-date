//
//  SettingsView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI

struct SettingsView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View {
        ZStack{
            LinearGradient(colors: [Color.init(hex: "#623F8E"), Color.init(hex: "#2B0648")], startPoint: .top, endPoint: .bottom)
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 0){
                CustomHeaderSettingsView()
                    .padding(.bottom, 15)
                
                HStack{
                    Text("Settings".localized(languages))
                        .fontWeight(.heavy)
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .padding([.leading, .top], 16)
                    
                    Spacer(minLength: 0)
                }
                .padding(.bottom, 20)
                
                BlockSettingsList()
                
                Spacer(minLength: 0)
            }
        }
        .navigationBarHidden(true)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
