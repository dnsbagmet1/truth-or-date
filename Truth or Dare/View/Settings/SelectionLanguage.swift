//
//  SelectionLanguage.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 27.08.2022.
//

import SwiftUI

struct SelectionLanguage: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View {
        ZStack{
            
            LinearGradient(colors: [Color.init(hex: "#623F8E"), Color.init(hex: "#2B0648")], startPoint: .top, endPoint: .bottom)
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 0){
                
                CustomHeaderSettingsView()
                    .padding(.bottom, 15)
                
                HStack{
                    Text("Language".localized(languages))
                        .fontWeight(.heavy)
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .padding([.leading, .top], 16)
                    
                    Spacer(minLength: 0)
                }
                .padding(.bottom, 20)
                
                BlockSettingsListLanguage()
                
                Spacer(minLength: 0)
            }
        }
        .navigationBarHidden(true)
    }
}

struct BlockSettingsListLanguage: View{
    var body: some View{
        VStack(spacing: 2){
            ItemListLanguage(language: .english, type: .CropUP)
            ItemListLanguage(language: .russian, type: .CropUPAndBottom)
            ItemListLanguage(language: .french, type: .CropUPAndBottom)
            ItemListLanguage(language: .german, type: .CropUPAndBottom)
            ItemListLanguage(language: .spanish, type: .CropBottom)
        }
    }
}

struct ItemListLanguage: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    var language: Language
    var type: TypeListItem = .Normal
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 11.71
        let sizeIconArrow = UIScreen.main.bounds.width / 15.62
        
        let sizeWidthPanel = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeightPanel = sizeWidthPanel / 5
        
        ZStack{
            
            Color.white.opacity(0.2)
            
            Button(action: {
                LocalizationService.shared.language = language
            }, label: {
                HStack(spacing: 10){
                    Image(getFlagByLanguage())
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIcon)
                    
                    Text(getNameLanguage())
                        .fontWeight(.semibold)
                        .font(.title2)
                        .foregroundColor(.white)
                    
                    Spacer(minLength: 0)
                    
                    Image(systemName: languages == language ? "record.circle" : "circle")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIconArrow)
                        .foregroundColor(.white)
                        .opacity(0.5)
                }
                .contentShape(Rectangle())
            })
            .buttonStyle(.plain)
            .padding(.horizontal, 16)
        }
        .frame(width: sizeWidthPanel, height: sizeHeightPanel)
        .cornerRadius(radius: 16, corners: getCornersByTypeListItem())
    }
    
    func getCornersByTypeListItem() -> UIRectCorner{
        switch type {
        case .Normal:
            return [.allCorners]
        case .CropUP:
            return [.topLeft, .topRight]
        case .CropBottom:
            return [.bottomLeft, .bottomRight]
        default:
            return []
        }
    }
    
    func getNameLanguage() -> String{
        switch language {
        case .english:
            return "English".localized(languages)
        case .russian:
            return "Russian".localized(languages)
        case .french:
            return "French".localized(languages)
        case .german:
            return "German".localized(languages)
        case .spanish:
            return "Spanish".localized(languages)
        }
    }
    
    func getFlagByLanguage() -> String{
        switch language {
        case .english:
            return "ImageFlagEn"
        case .russian:
            return "ImageFlagRu"
        case .french:
            return "ImageFlagFr"
        case .german:
            return "ImageFlagGe"
        case .spanish:
            return "ImageFlagSp"
        }
    }
}

struct SelectionLanguage_Previews: PreviewProvider {
    static var previews: some View {
        SelectionLanguage()
    }
}
