//
//  ItemList.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 28.08.2022.
//

import SwiftUI

struct ItemList: View{
    var image: String = ""
    var title: String = ""
    var type: TypeListItem = .Normal
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 11.71
        let sizeIconArrow = UIScreen.main.bounds.width / 15.62
        
        let sizeWidthPanel = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeightPanel = sizeWidthPanel / 5
        
        ZStack{
            
            Color.white.opacity(0.2)
            
            HStack(spacing: 10){
                Image(image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIcon)
                
                Text(title)
                    .fontWeight(.semibold)
                    .font(.title2)
                    .foregroundColor(.white)
                
                Spacer(minLength: 0)
                
                Image("ImageArrowShort")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIconArrow)
            }
            .padding(.horizontal, 16)
        }
        .frame(width: sizeWidthPanel, height: sizeHeightPanel)
        .cornerRadius(radius: 16, corners: getCornersByTypeListItem())
    }
    
    func getCornersByTypeListItem() -> UIRectCorner{
        switch type {
        case .Normal:
            return [.allCorners]
        case .CropUP:
            return [.topLeft, .topRight]
        case .CropBottom:
            return [.bottomLeft, .bottomRight]
        default:
            return []
        }
    }
}

struct ItemList_Previews: PreviewProvider {
    static var previews: some View {
        ItemList()
    }
}
