//
//  Thruth.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import SwiftUI

struct QuestionView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var gameData: GameViewModel
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State var timerSeconds: Int = 60
    var body: some View {
        ZStack{
            
            if gameData.questionType == .Truth{
                LinearGradient(colors: [Color.init(hex: "6E17B2"), Color.init(hex: "1C0738")], startPoint: .top, endPoint: .bottom)
                    .ignoresSafeArea()
                    .onReceive(timer, perform: { _ in
                        if timerSeconds > 0{
                            timerSeconds -= 1
                        }else{
                            
                            self.gameData.players[self.gameData.currentPlayerIndex].countCry += 1
                            self.gameData.checkFinishGame()
                            self.gameData.nextPlayer()
                            presentationMode.wrappedValue.dismiss()
                        }
                    })
                    .onDisappear(perform: {
                        timer.upstream.connect().cancel()
                    })
            }else{
                LinearGradient(colors: [Color.init(hex: "FE327B"), Color.init(hex: "2B0648")], startPoint: .top, endPoint: .bottom)
                    .ignoresSafeArea()
            }
            
            VStack(spacing: 0){
                Text("\(timerSeconds)")
                    .font(.custom("NunitoSans-Light", size: 70))
                    .fontWeight(.light)
                    .font(.largeTitle)
                    .opacity(gameData.questionType == .Truth ? 1 : 0)
                
                Text(gameData.questionType == .Truth ? "Truth".localized(languages) : "Action".localized(languages))
                    .fontWeight(.heavy)
                    .font(.largeTitle)
                    .padding(.top, 50)
                
                Text("\(self.gameData.getNameCurrentPlayer()) " + "move".localized(languages))
                    .fontWeight(.semibold)
                    .font(.title3)
                    .opacity(0.5)
                    .padding(.top, 8)
                
                Divider()
                    .background(Color.white.opacity(0.2))
                    .padding(.top, 30)
                
                Text(gameData.currentTask)
                    .fontWeight(.semibold)
                    .font(.title3)
                    .padding(.top, 10)
                
                VStack(spacing: 15){
                    Button {
                        self.gameData.addScoreToCurrentPlayer()
                        if gameData.questionType == .Truth{
                            self.gameData.players[self.gameData.currentPlayerIndex].countAnswerTruth += 1
                        }else{
                            self.gameData.players[self.gameData.currentPlayerIndex].countAnswerDare += 1
                        }
                        
                        self.gameData.checkFinishGame()
                        self.gameData.nextPlayer()
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        ButtonDone()
                    }
                    .buttonStyle(.plain)
                    
                    
                    Button {
                        self.gameData.players[self.gameData.currentPlayerIndex].countCry += 1
                        self.gameData.checkFinishGame()
                        self.gameData.nextPlayer()
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        ButtonCry()
                    }
                    .buttonStyle(.plain)
                    
                }
                .padding(.top, 85)
            }
            .foregroundColor(.white)
            .padding(.horizontal, 16)
        }
        .navigationBarHidden(true)
    }
}

struct Thruth_Previews: PreviewProvider {
    static var previews: some View {
        QuestionView().environmentObject(GameViewModel())
    }
}
