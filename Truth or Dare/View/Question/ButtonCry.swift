//
//  ButtonCry.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct ButtonCry: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        let widthSize = UIScreen.main.bounds.width - (38 * 2)
        let heightSize = widthSize / 4.05
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "FF3131"), Color.init(hex: "FF5F5F")], startPoint: .topLeading, endPoint: .bottomTrailing)
            
            HStack{
                Spacer(minLength: 0)
                
                Text("Surrender".localized(languages))
                    .fontWeight(.black)
                    .font(.title3)
                    .foregroundColor(.white)
                
                Spacer(minLength: 0)
            }
            
            HStack(spacing: 0){
                Spacer(minLength: 0)
                
                Color.white
                    .frame(width: 1, height: heightSize)
                
                Text("0")
                    .font(.title3)
                    .foregroundColor(.white)
                    .padding(.horizontal, 18)
                    .padding(.leading, 7)
            }
        }
        .frame(width: widthSize, height: heightSize)
        .cornerRadius(24)
    }
}

struct ButtonCry_Previews: PreviewProvider {
    static var previews: some View {
        ButtonCry()
    }
}
