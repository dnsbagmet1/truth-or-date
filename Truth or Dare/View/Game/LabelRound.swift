//
//  LabelRound.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct LabelRound: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        
        let widthSize = UIScreen.main.bounds.width / 4.62
        let heightSize = widthSize / 1.62
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "#CE5151"), Color.init(hex: "#DE335C")], startPoint: .leading, endPoint: .trailing)
            
            Text("Round".localized(languages))
            .fontWeight(.black)
            .font(.title3)
            .foregroundColor(.white)
        }
        .frame(width: widthSize, height: heightSize)
        .clipShape(Capsule())
    }
}

struct LabelRound_Previews: PreviewProvider {
    static var previews: some View {
        LabelRound()
    }
}
