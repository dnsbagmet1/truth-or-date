//
//  CustomHeaderGameView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomHeaderGameView: View{
    @EnvironmentObject var gameData: GameViewModel
    @Binding var showMenu: Bool
    @Binding var showStatistic: Bool
    @Binding var showHelpView: Bool
    
    var body: some View{
        
        let sizeIconMenu = UIScreen.main.bounds.width / 6.57
        let sizeIconHelp = UIScreen.main.bounds.width / 9.14
        
        ZStack{
            HStack{
                Button {
                    withAnimation{
                        showMenu.toggle()
                    }
                } label: {
                    Image("ImageMenu")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIconMenu)
                }
                .buttonStyle(.plain)
                
                Spacer(minLength: 0)
            }
            
            HStack{
                Button {
                    withAnimation {
                        showStatistic.toggle()
                    }
                } label: {
                    ZStack{
                        
                        Circle()
                            .stroke(lineWidth: 2)
                            .foregroundColor(.white)
                            .frame(width: sizeIconMenu, height: sizeIconMenu)
                        
                        
                        Text("\(gameData.countRounds)")
                            .fontWeight(.semibold)
                            .font(.title)
                            .foregroundColor(.white)
                    }
                }
                .buttonStyle(.plain)
            }
            
            HStack{
                Spacer(minLength: 0)
                
                Button {
                    withAnimation {
                        showHelpView.toggle()
                    }
                } label: {
                    Image("ImageHelp")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIconHelp)
                }
                .buttonStyle(.plain)
            }
        }
        .padding(.horizontal, 16)
    }
}

struct CustomHeaderGameView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack{
            Color.black
            
            CustomHeaderGameView(showMenu: .constant(false), showStatistic: .constant(true), showHelpView: .constant(false))
        }
        .environmentObject(GameViewModel())
    }
}
