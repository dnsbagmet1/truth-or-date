//
//  ButtonDare.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct ButtonDare: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        let widthSize = UIScreen.main.bounds.width - (38 * 2)
        let heightSize = widthSize / 4.4
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "FA6C30"), Color.init(hex: "FF9900")], startPoint: .leading, endPoint: .trailing)
            
            Text("Action".localized(languages))
            .fontWeight(.black)
            .font(.title3)
            .foregroundColor(.white)
        }
        .frame(width: widthSize, height: heightSize)
        .cornerRadius(24)
    }
}


struct ButtonDare_Previews: PreviewProvider {
    static var previews: some View {
        ButtonDare()
    }
}
