//
//  ButtonRandom.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct ButtonRandom: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        let widthSize = UIScreen.main.bounds.width - (38 * 2)
        let heightSize = widthSize / 4.4
        
        ZStack{
            Text("By chance".localized(languages))
            .fontWeight(.black)
            .font(.title3)
            .foregroundColor(.white)
        }
        .frame(width: widthSize, height: heightSize)
        .overlay(
            RoundedRectangle(cornerRadius: 24)
                .stroke(.white, lineWidth: 3)
        )
    }
}

struct ButtonRandom_Previews: PreviewProvider {
    static var previews: some View {
        ButtonRandom()
    }
}
