//
//  CustomButtonNewGame.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomButtonNewGame: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        let widthSize = UIScreen.main.bounds.width - (38 * 2)
        let heightSize = widthSize / 4.41
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "3AA813"), Color.init(hex: "4EBE27")], startPoint: .top, endPoint: .bottom)
            
            Text("New game".localized(languages))
            .fontWeight(.black)
            .font(.title3)
            .foregroundColor(.white)
        }
        .frame(width: widthSize, height: heightSize)
        .cornerRadius(24)
    }
}

struct CustomButtonNewGame_Previews: PreviewProvider {
    static var previews: some View {
        CustomButtonNewGame()
    }
}
