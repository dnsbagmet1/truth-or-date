//
//  StatisticPlayers.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 02.09.2022.
//

import SwiftUI

struct StatisticPlayers: View {
    @EnvironmentObject var gameData: GameViewModel
    @State private var contentSize: CGSize = .zero
    var body: some View {
        
        let widthSize = UIScreen.main.bounds.width - (16 * 2)
        
        VStack(spacing: -3){
            Image(systemName: "triangle.fill")
                .foregroundColor(.white)
            ZStack{
                Color.white
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 0){
                        LabelRound()
                            .padding(.top, 20)
                        
                        VStack(spacing: 8){
                            ForEach(gameData.players, id: \.id){player in
                                ItemListPlayerStatistic(player: player)
                            }
                        }
                        .padding(.top, 20)
                        .padding(.bottom, 15)
                    }
                    .overlay(GeometryReader{reader in
                        Color.clear.onAppear {
                            if reader.size.height < UIScreen.main.bounds.height / 1.35{
                                contentSize = reader.size
                            }else{
                                contentSize.height =  UIScreen.main.bounds.height / 1.35
                            }
                        }
                    })
                }
            }
            .frame(height: contentSize.height == .zero ? UIScreen.main.bounds.height : contentSize.height)
            .frame(width: widthSize)
            .cornerRadius(16)
        }
    }
}

struct StatisticPlayers_Previews: PreviewProvider {
    static var previews: some View {
        StatisticPlayers()
            .environmentObject(GameViewModel())
    }
}
