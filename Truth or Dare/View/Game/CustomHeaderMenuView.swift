//
//  CustomHeaderMenuView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomHeaderMenuView: View{
    @Binding var showMenuView: Bool
    var body: some View{
        HStack{
            Button {
                withAnimation {
                    showMenuView.toggle()
                }
            } label: {
                CustomButtonBack()
            }
            .buttonStyle(.plain)
            
            Spacer(minLength: 0)

        }
        .padding(.horizontal, 16)
    }
}

struct CustomHeaderMenuView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderMenuView(showMenuView: .constant(false))
    }
}
