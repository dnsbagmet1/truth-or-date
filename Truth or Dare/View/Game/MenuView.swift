//
//  MenuView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct MenuView: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @Binding var showMenuView: Bool
    @Binding var openGameView: Bool
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 6.25
        
        ZStack{
            VStack(spacing: 0){
                CustomHeaderMenuView(showMenuView: $showMenuView)
                
                VStack(spacing: 0){
                    Text("Menu".localized(languages))
                    .fontWeight(.bold)
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    
                    Image("ImageLinesMenu")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIcon)
                        .padding(.top, 30)
                    
                    VStack(spacing: 20){
                        Button {
                            withAnimation {
                                showMenuView.toggle()
                            }
                        } label: {
                            CustomButtonProceed()
                        }
                        .buttonStyle(.plain)
                        
                        Button {
                            openGameView.toggle()
                        } label: {
                            CustomButtonNewGame()
                        }
                        .buttonStyle(.plain)
                    }
                    .padding(.top, 40)

                }
                .padding(.top, 65)
                
                Spacer(minLength: 0)
            }
        }
        .background(VisualEffectView(effect: UIBlurEffect(style: .systemThinMaterialDark)).ignoresSafeArea())
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView(showMenuView: .constant(false), openGameView: .constant(true))
    }
}
