//
//  GameView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 25.08.2022.
//

import SwiftUI
import StoreKit

struct GameView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @EnvironmentObject var gameData: GameViewModel
    @State var showMenuView: Bool = false
    @State var showHelpView: Bool = false
    @State var showStatistic: Bool = false
    @State var openQuestionView: Bool = false
    
    @Binding var openGameView: Bool
    
    var body: some View {
        ZStack{
            
            NavigationLink(isActive: $openQuestionView) {
                QuestionView()
            } label: {
                EmptyView()
            }
            
            LinearGradient(colors: [Color.init(hex: "#423486"), Color.init(hex: "#39064A")], startPoint: .top, endPoint: .bottom)
                .ignoresSafeArea()
            
            VStack(spacing: 0){
                CustomHeaderGameView(showMenu: $showMenuView, showStatistic: $showStatistic, showHelpView: $showHelpView)
                
                Spacer(minLength: 0)
                
                ZStack{
                    LinearGradient(colors: [Color.init(hex: "#5E4CA8"), Color.init(hex: "#391765")], startPoint: .top, endPoint: .bottom)
                    
                    VStack{
                        
                        HStack{
                            Spacer(minLength: 0)
                            
                            Circle()
                                .foregroundColor(Color.init(hex: "#ED1AFF"))
                                .frame(width: 200, height: 200)
                                .blur(radius: 80)
                        }
                        
                        Spacer(minLength: 0)
                        
                        HStack{
                            Spacer(minLength: 0)
                            
                            Circle()
                                .foregroundColor(Color.init(hex: "#09FFFF"))
                                .frame(width: 200, height: 200)
                                .blur(radius: 100)
                        }
                    }
                    
                    VStack{
                        VStack(spacing: 7){
                            Text(self.gameData.getNameCurrentPlayer())
                                .fontWeight(.bold)
                                .font(.largeTitle)
                            
                            Text("Your turn".localized(languages))
                                .font(.title3)
                        }
                        .foregroundColor(.white)
                        .padding(.top, 65)
                        
                        
                        VStack(spacing: 20){
                            Button {
                                gameData.questionType = .Truth
                                gameData.getTask()
                                openQuestionView.toggle()
                            } label: {
                                ButtonTruth()
                            }
                            .buttonStyle(.plain)
                            
                            
                            Button {
                                gameData.questionType = .Dare
                                gameData.getTask()
                                openQuestionView.toggle()
                            } label: {
                                ButtonDare()
                            }
                            .buttonStyle(.plain)
                            
                            
                            Button {
                                if Int.random(in: 0..<2) == 0{
                                    gameData.questionType = .Truth
                                }else{
                                    gameData.questionType = .Dare
                                }
                                gameData.getTask()
                                openQuestionView.toggle()
                            } label: {
                                ButtonRandom()
                            }
                            .buttonStyle(.plain)
                            
                        }
                        .padding(.top, 65)
                        
                        Spacer(minLength: 0)
                    }
                }
                .cornerRadius(radius: 40, corners: [.topLeft, .topRight])
                .padding(.top, 110)
            }
            .overlay{
                let paddingTop = UIScreen.main.bounds.height / 13
                
                if showStatistic{
                    VStack{
                        
                        StatisticPlayers()
                        
                        Spacer(minLength: 0)
                    }
                    .padding(.top, paddingTop)
                }
            }
            .overlay{
                if showMenuView{
                    MenuView(showMenuView: $showMenuView, openGameView: $openGameView)
                }
            }
            
        }
        .alert(isPresented: $gameData.isFinishedGame, content: {
            return Alert(
                title: Text("Game over".localized(languages)),
                message: Text("\(gameData.getNameCurrentWin()) " + "Win".localized(languages)),
                dismissButton: .default(Text("Ok".localized(languages)), action: {
                    openGameView = false
                }))
        })
        .edgesIgnoringSafeArea(.bottom)
        .bottomSheet(isPresented: $showHelpView, isHandler: false, content: {
            RuleView(showHelpView: $showHelpView)
        })
        .onChange(of: gameData.isFinishedGame, perform: { _ in
            if let windowScene = UIApplication.shared.windows.first?.windowScene { SKStoreReviewController.requestReview(in: windowScene) }
        })
        .navigationBarHidden(true)
    }
}

struct VisualEffectView: UIViewRepresentable {
    var effect: UIVisualEffect?
    func makeUIView(context: UIViewRepresentableContext<Self>) -> UIVisualEffectView { UIVisualEffectView() }
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Self>) { uiView.effect = effect }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(openGameView: .constant(true)).environmentObject(GameViewModel())
    }
}
