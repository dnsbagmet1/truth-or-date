//
//  ItemListPlayerStatistic.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 28.08.2022.
//

import SwiftUI

struct ItemListPlayerStatistic: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @EnvironmentObject var gameData: GameViewModel
    var player: PlayerModel
    var body: some View{
        
        let widthSize = UIScreen.main.bounds.width - (30 * 2)
        let heightSize = widthSize / 4.55
        
        ZStack{
            if checkCurrentUserSelected(){
                LinearGradient(colors: [Color.init(hex: "DB5A3D"), Color.init(hex: "6B2982")], startPoint: .leading, endPoint: .trailing)
            }else{
                Color.init(hex: "#F3E9E6")
            }
            
            HStack(spacing: 0){
                Text("\(getIndexPlayer())")
                    .fontWeight(.semibold)
                    .font(.title2)
                    .foregroundColor(checkCurrentUserSelected() ? .white : .black)
                
                VStack(alignment: .leading, spacing: 5){
                    HStack{
                        
                        Text(player.name)
                            .fontWeight(.bold)
                            .font(.title2)
                            .foregroundColor(checkCurrentUserSelected() ? .white : .black)
                        
                        Spacer(minLength: 0)
                        
                        Text("\(player.score) " + "points".localized(languages))
                            .fontWeight(.bold)
                            .font(.body)
                            .foregroundColor(checkCurrentUserSelected() ? .white : .black)
                    }
                    
                    HStack(alignment: .top, spacing: 5){
                        Text("\(player.countAnswerTruth) " + "truth".localized(languages))
                            .fontWeight(.semibold)
                            .font(.footnote)
                        
                        Color.black
                            .frame(width: 1, height: 20)
                        
                        Text("\(player.countAnswerDare) " + "action".localized(languages))
                            .fontWeight(.semibold)
                            .font(.footnote)
                        
                        Color.black
                            .frame(width: 1, height: 20)
                        
                        Text("\(player.countCry) " + "surrendered".localized(languages))
                            .fontWeight(.semibold)
                            .font(.footnote)
                    }
                    .foregroundColor(checkCurrentUserSelected() ? .white : .black)
                    .opacity(0.5)
                }
                .padding(.leading, 15)
                
                Spacer(minLength: 0)
            }
            .padding(.leading, 20)
            .padding(.trailing, 35)
        }
        .frame(width: widthSize, height: heightSize)
        .cornerRadius(16)
    }
    
    func getIndexPlayer() -> Int{
        if let index = gameData.players.firstIndex(where: {$0.id.uuidString == player.id.uuidString}){
            return index + 1
        }
        
        return 1
    }
    
    func checkCurrentUserSelected() -> Bool{
        return gameData.players[gameData.currentPlayerIndex].id.uuidString == player.id.uuidString
    }
}

struct ItemListPlayerStatistic_Previews: PreviewProvider {
    static var previews: some View {
        ItemListPlayerStatistic(player: PlayerModel(name: "Denis", score: 0, countAnswerTruth: 0, countAnswerDare: 0, countCry: 0))
            .environmentObject(GameViewModel())
    }
}
