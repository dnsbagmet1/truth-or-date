//
//  ItemSelectionListSets.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct ItemSelectionListSets: View{
    @EnvironmentObject var subscriptionsData: SubscriptionViewModel
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @EnvironmentObject var gameData: GameViewModel
    @Binding var previewQuestionView: Bool
    @Binding var openGame: Bool
    var type: SetsType
    var body: some View{
        
        let sizeImage = (UIScreen.main.bounds.width - (16 * 2) - 10) / 2
        
        Button {
            gameData.currentSet = type
            
            if type == .WarmUp{
                gameData.getTask()
                openGame.toggle()
            }else{
                if subscriptionsData.isPremium{
                    gameData.getTask()
                    openGame.toggle()
                }else{
                    gameData.getRandomPreviewTask()
                    withAnimation {
                        previewQuestionView.toggle()
                    }
                }
            }
        } label: {
            VStack(alignment: .leading, spacing: 0){
                ZStack(alignment: .bottom){
                    
                    Image(gameData.getImageBySet(set: type))
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeImage)
                    
                    Text(gameData.getNameBySet(set: type))
                    .fontWeight(.heavy)
                    .font(.body)
                    .padding(.bottom, 8)
                }
                
                Text("\(gameData.getCountQuestions(type: type)) " + "cards".localized(languages))
                    .fontWeight(.heavy)
                    .font(.subheadline)
                    .lineSpacing(18)
                    .padding(.leading, 12)
                    .padding(.vertical, 15)
                    .foregroundColor(Color(red: 0.93, green: 0.19, blue: 0.46))
            }
            .background(Color.white)
            .cornerRadius(20)
        }
        .buttonStyle(.plain)
        
    }
}

struct ItemSelectionListSets_Previews: PreviewProvider {
    static var previews: some View {
        ItemSelectionListSets(previewQuestionView: .constant(false), openGame: .constant(false), type: .BachelorParty)
    }
}
