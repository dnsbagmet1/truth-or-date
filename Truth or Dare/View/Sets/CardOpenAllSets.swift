//
//  CardOpenAllSets.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct CardOpenAllSets: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        
        let sizeImage = UIScreen.main.bounds.width - (28 * 2)
        let widthSize = UIScreen.main.bounds.width - (16 * 2)
        let heighttSize = widthSize / 2.11
        
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "#FD833F"), Color.init(hex: "#ED167D")], startPoint: .leading, endPoint: .trailing)
            
            Image("ImageCards")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: sizeImage)
            
            VStack(spacing: 0){
                Text("Unlock all sets".localized(languages))
                    .fontWeight(.heavy)
                    .font(.title)
                    .padding(.top, 10)
                
                Text("Open 2000+ cards now".localized(languages))
                    .fontWeight(.semibold)
                    .font(.callout)
                
                Spacer(minLength: 0)
            }
            .foregroundColor(.white)
            
        }
        .frame(width: widthSize, height: heighttSize)
        .cornerRadius(16)
    }
}


struct CardOpenAllSets_Previews: PreviewProvider {
    static var previews: some View {
        CardOpenAllSets()
    }
}
