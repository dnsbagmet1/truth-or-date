//
//  CustomHeaderSelectionSetsView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct CustomHeaderSelectionSetsView: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @Environment(\.presentationMode) var presentationMode
    @Binding var showHelpView: Bool
    var body: some View{
        
        let sizeIconHelp = UIScreen.main.bounds.width / 9.14
        
        ZStack{
            HStack{
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    CustomButtonBack()
                }
                .buttonStyle(.plain)
                
                Spacer(minLength: 0)
            }
            
            HStack{
                Text("Select kits".localized(languages))
                    .fontWeight(.bold)
                    .font(.title2)
                    .lineSpacing(50)
                    .foregroundColor(.white)
            }
            
            HStack{
                Spacer(minLength: 0)
                
                Button {
                    withAnimation {
                        showHelpView.toggle()
                    }
                } label: {
                    Image("ImageHelp")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIconHelp)
                }
                .buttonStyle(.plain)
                
            }
        }
        .padding(.horizontal, 16)
    }
}

struct CustomHeaderSelectionSetsView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderSelectionSetsView(showHelpView: .constant(false))
    }
}
