//
//  BlockSelectionListSets.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct BlockSelectionListSets: View{
    @EnvironmentObject var subscriptionsData: SubscriptionViewModel
    @Binding var openGame: Bool
    @Binding var previewQuestionView: Bool
    
    var body: some View{
        VStack(spacing: 16){
            HStack(spacing: 10){
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .WarmUp)
                
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .Contact)
            }
            
            if !subscriptionsData.isPremium{
                Button {
                    subscriptionsData.buyProduct(productType: .nonConsumablePurchaseForever)
                } label: {
                    CardOpenAllSets()
                }
                .buttonStyle(.plain)
            }
            
            HStack(spacing: 10){
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .HenParty)
                    
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .BachelorParty)
            }
            
            HStack(spacing: 10){
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .Hangout)
                
                ItemSelectionListSets(previewQuestionView: $previewQuestionView, openGame: $openGame, type: .Hardcor)
            }
        }
    }
}

struct BlockSelectionListSets_Previews: PreviewProvider {
    static var previews: some View {
        BlockSelectionListSets(openGame: .constant(false), previewQuestionView: .constant(false))
    }
}
