//
//  SelectionSetsView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import SwiftUI

struct SetsView: View {
    @EnvironmentObject var subscriptionsData: SubscriptionViewModel
    @State var showHelpView: Bool = false
    @State var openGame: Bool = false
    
    @State var previewQuestionView: Bool = false
    
    @Binding var openGameView: Bool
    
    var body: some View {
        ZStack{
            
            NavigationLink(isActive: $openGame) {
                GameView(openGameView: $openGameView)
            } label: {
                EmptyView()
            }
            
            NavigationLink(isActive: $subscriptionsData.openPremium) {
                PremiumView()
            } label: {
                EmptyView()
            }

            
            LinearGradient(colors: [Color.init(hex: "471D8B"), Color.init(hex: "2B0A36")], startPoint: .top, endPoint: .bottom)
                .ignoresSafeArea()
            
            VStack(spacing: 16){
                CustomHeaderSelectionSetsView(showHelpView: $showHelpView)
                
                ScrollView{
                    BlockSelectionListSets(openGame: $openGame, previewQuestionView: $previewQuestionView)
                }
            }
        }
        .bottomSheet(isPresented: $showHelpView, isHandler: false, content: {
            RuleView(showHelpView: $showHelpView)
        })
        .bottomSheet(isPresented: $previewQuestionView, isHandler: false, content: {
            PreviewQuestionView(previewQuestionView: $previewQuestionView)
        })
        .navigationBarHidden(true)
    }
}


struct SelectionSetsView_Previews: PreviewProvider {
    static var previews: some View {
        SetsView(openGameView: .constant(true))
    }
}
