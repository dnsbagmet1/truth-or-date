//
//  CustomTextField.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomTextField: View{
    @EnvironmentObject var gameData: GameViewModel
    @Binding var playerName: String
    var indexPlayer: Int = 0
    var additionalPlayer: Bool = false
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 14
        let sizeWidthPanel = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeightPanel = sizeWidthPanel / 5
        
        ZStack{
            Color.white
            
            HStack(spacing: 10){
                Image("ImageGameController")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIcon)
                
                HStack(spacing: 10){
                    
                    TextField("Player" + " \(indexPlayer + 1)", text: $playerName)
                        .font(Font.title2)
                        .font(Font.body.weight(.semibold))
                        .disableAutocorrection(true)
                        .foregroundColor(Color.init(hex: "#471068"))
                    
                    
                    if additionalPlayer{
                        Button {
                            //HACK
                            playerName = "\(Float.random(in: 0..<100))"
                            if let index = gameData.players.firstIndex(where: {$0.name == playerName}){
                                gameData.players.remove(at: index)
                            }
                        } label: {
                            Image(systemName: "xmark.circle.fill")
                                .imageScale(.large)
                                .foregroundColor(Color.init(hex: "#471068"))
                                .opacity(0.2)
                        }
                        .buttonStyle(.plain)
                    }
                }
            }
            .padding(.horizontal, 16)
        }
        .frame(width: sizeWidthPanel, height: sizeHeightPanel)
        .cornerRadius(16)
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextField(playerName: .constant("Denis")).environmentObject(GameViewModel())
    }
}
