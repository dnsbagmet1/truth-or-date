//
//  ButtonStartGame.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct ButtonStartGame: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 12
        let sizeWidthPanel = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeightPanel = sizeWidthPanel / 5
        
        ZStack{
            Color("ColorOrange")
            
            HStack(spacing: 10){
                
                Spacer(minLength: 0)
                
                Text("Start the fun".localized(languages))
                    .fontWeight(.black)
                    .font(.title3)
                
                Image("ImageArrow")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIcon)
                
                Spacer(minLength: 0)
            }
            .foregroundColor(.white)
        }
        .frame(width: sizeWidthPanel, height: sizeHeightPanel)
        .cornerRadius(16)
    }
}


struct ButtonStartGame_Previews: PreviewProvider {
    static var previews: some View {
        ButtonStartGame()
    }
}
