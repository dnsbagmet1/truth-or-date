//
//  CustomHeaderHomeView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomHeaderHomeView: View{
    @Binding var showHelpView: Bool
    var body: some View{
        
        let sizeIconSettings = UIScreen.main.bounds.width / 6.57
        let sizeIconHelp = UIScreen.main.bounds.width / 9.14
        
        HStack{
            NavigationLink {
                SettingsView()
            } label: {
                Image("ImageSettings")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIconSettings)
            }
            .buttonStyle(.plain)

            
            Spacer(minLength: 0)
            
            Button {
                withAnimation {
                    showHelpView.toggle()
                }
            } label: {
                Image("ImageHelp")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIconHelp)
            }
            .buttonStyle(.plain)
        }
        .padding(.horizontal, 16)
    }
}

struct CustomHeaderHomeView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderHomeView(showHelpView: .constant(false))
    }
}
