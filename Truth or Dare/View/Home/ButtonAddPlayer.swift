//
//  ButtonAddPlayer.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct ButtonAddPlayer: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 14
        let sizeWidthPanel = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeightPanel = sizeWidthPanel / 5
        
        ZStack{
            
            Color.white.opacity(0.2)
            
            HStack(spacing: 10){
                Image("ImagePlus")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIcon)
                
                Text("Add player".localized(languages))
                    .fontWeight(.semibold)
                    .font(.title2)
                    .foregroundColor(.white)
            }
        }
        .frame(width: sizeWidthPanel, height: sizeHeightPanel)
        .cornerRadius(16)
    }
}

struct ButtonAddPlayer_Previews: PreviewProvider {
    static var previews: some View {
        ButtonAddPlayer()
    }
}
