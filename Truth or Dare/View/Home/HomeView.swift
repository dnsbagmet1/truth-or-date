//
//  HomeView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI

struct HomeView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @EnvironmentObject var gameData: GameViewModel
    @State var showHelpView: Bool = false
    @State var openGameView: Bool = false
    var body: some View {
        NavigationView{
            ZStack{
                
                LinearGradient(colors: [Color.init(hex: "#6715A8"), Color.init(hex: "#DF2B73")], startPoint: .top, endPoint: .bottom)
                    .ignoresSafeArea()
                
                VStack(spacing: 0){
                    CustomHeaderHomeView(showHelpView: $showHelpView)
                    
                    Text("Truth or Dare".localized(languages))
                        .fontWeight(.heavy)
                        .font(.largeTitle)
                        .multilineTextAlignment(.center)
                        .foregroundColor(.white)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.top, 24)
                        .padding(.bottom, 18)
                    
                    VStack(spacing: 10){
                        ScrollView(.vertical, showsIndicators: false) {
                            ScrollViewReader{reader in
                                ForEach(gameData.players.indices, id: \.self) { indexPlayer in
                                    CustomTextField(playerName: $gameData.players[indexPlayer].name, indexPlayer: indexPlayer, additionalPlayer: indexPlayer > 1)
                                    
                                }
                                .onChange(of: gameData.players.count) { _ in
                                    reader.scrollTo("btnNewPlayer")
                                }
                                
                                if gameData.players.count < 10{
                                    Button {
                                        gameData.addNewPlayer()
                                    } label: {
                                        ButtonAddPlayer()
                                    }
                                    .buttonStyle(.plain)
                                    .id("btnNewPlayer")
                                }
                            }
                        }
                    }
                    .padding(.bottom, 10)
                    
                    
                    Spacer(minLength: 0)
                    
                    
                    NavigationLink(isActive: $openGameView) {
                        SetsView(openGameView: $openGameView)
                    } label: {
                        EmptyView()
                    }
                    .isDetailLink(false)

                    Button {
                        
                        gameData.newGame()
                        
                        for i in 0..<gameData.players.count {
                            if gameData.players[i].name.isEmpty{
                                gameData.players[i].name = "Player".localized(languages) + " \(i + 1)"
                            }
                        }
                        
                        openGameView.toggle()
                    } label: {
                        ButtonStartGame()
                    }
                    .buttonStyle(.plain)
                    .padding(.bottom, 30)
                    
                    CustomBottomPanelHomeView()
                        .padding(.bottom, 25)
                }
            }
            .bottomSheet(isPresented: $showHelpView, isHandler: false, content: {
                RuleView(showHelpView: $showHelpView)
            })
            .ignoresSafeArea(.keyboard, edges: .all)
            .navigationBarHidden(true)
        }
        .navigationViewStyle(.stack)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environmentObject(GameViewModel())
    }
}
