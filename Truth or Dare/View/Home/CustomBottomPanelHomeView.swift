//
//  CustomBottomPanelHomeView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct CustomBottomPanelHomeView: View{
    @Environment(\.openURL) var openURL
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 6.57
        
        HStack{
            Spacer(minLength: 0)
            
            HStack(spacing: 18){
                Button {
                    openURL(URL(string: "https://google.com")!)
                } label: {
                    Image("ImageRating")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIcon)
                }
                .buttonStyle(.plain)
                
                Button {
                    openURL(URL(string: "https://google.com")!)
                } label: {
                    Image("ImageReview")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIcon)
                }
                .buttonStyle(.plain)
                
                
                Button {
                    actionSheetShare(url: "https://google.com")
                } label: {
                    Image("ImageShare")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: sizeIcon)
                }
                .buttonStyle(.plain)
                
            }
            
            Spacer(minLength: 0)
        }
    }
    
    func actionSheetShare(url: String) {
        guard let data = URL(string: url) else { return }
        let av = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true, completion: nil)
    }
}

struct CustomBottomPanelHomeView_Previews: PreviewProvider {
    static var previews: some View {
        CustomBottomPanelHomeView()
    }
}
