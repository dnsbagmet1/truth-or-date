//
//  CustomButtonContinue.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct CustomButtonContinue: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        
        let widthIcon = UIScreen.main.bounds.width / 11.72
        let sizeWidth = UIScreen.main.bounds.width - (16 * 2)
        let sizeHeight = sizeWidth / 4.57
        
        ZStack{
            Color.init(hex: "FF9900")
            
            HStack(spacing: 10){
                Text("Proceed".localized(languages))
                    .fontWeight(.black)
                    .font(.title3)
                    .foregroundColor(.white)
                
                Image("ImageArrow")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: widthIcon)
            }
        }
        .frame(width: sizeWidth, height: sizeHeight)
        .cornerRadius(24)
    }
}


struct CustomButtonContinue_Previews: PreviewProvider {
    static var previews: some View {
        CustomButtonContinue()
    }
}
