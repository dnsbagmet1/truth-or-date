//
//  CustomHeaderPremiumView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 10.09.2022.
//

import SwiftUI

struct CustomHeaderPremiumView: View{
    @Environment(\.presentationMode) var presentationMode
    var body: some View{
        HStack{
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                CustomButtonBack()
            }
            .buttonStyle(.plain)
            .padding(.leading, 16)
            
            Spacer(minLength: 0)
        }
    }
}

struct CustomHeaderPremiumView_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderPremiumView()
    }
}
