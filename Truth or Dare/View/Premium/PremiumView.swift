//
//  PremiumView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct PremiumView: View {
    @Environment(\.openURL) var openURL
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var subscriptionsData: SubscriptionViewModel
    
    @AppStorage("isPremium") var isPremium: Bool = false
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @State var isPeriodWeek: Bool = false
    
    var body: some View {
        
        let widthIcon = UIScreen.main.bounds.width / 9.87
        let widthIconProtected = UIScreen.main.bounds.width / 9.37
        
        ZStack{
            LinearGradient(colors: [Color.init(hex: "463F8E"), Color.init(hex: "2B0648")], startPoint: .top, endPoint: .bottom)
                .ignoresSafeArea(edges: .all)
            
            VStack(spacing: 0){
                CustomHeaderPremiumView()
                
                HStack{
                    Text("Full access".localized(languages))
                        .fontWeight(.heavy)
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .padding([.leading, .top], 16)
                    
                    Spacer(minLength: 0)
                }
                
                VStack(alignment: .leading, spacing: 6){
                    HStack(spacing: 10){
                        Image("ImageContactSupport")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: widthIcon)
                        
                        Text("All hot questions".localized(languages))
                            .fontWeight(.semibold)
                            .font(.title3)
                    }
                    
                    HStack(spacing: 10){
                        Image("ImageNoAds")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: widthIcon)
                        
                        Text("No ads 100%".localized(languages))
                            .fontWeight(.semibold)
                            .font(.title3)
                    }
                    
                    HStack(spacing: 10){
                        Image("ImageWeeklyUpadte")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: widthIcon)
                        
                        Text("Weekly updates".localized(languages))
                            .fontWeight(.semibold)
                            .font(.title3)
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 16)
                .foregroundColor(.white)
                .padding(.top, 15)
                
                VStack(spacing: 0){
                    if isPeriodWeek{
                        Text("Total".localized(languages) + " \(getPriceProduct()) / " + "week".localized(languages))
                            .fontWeight(.heavy)
                            .font(.title3)
                            .foregroundColor(.white)
                    }else{
                        Text("Total".localized(languages) + " \(getPriceProduct()) / " + "year".localized(languages))
                            .fontWeight(.heavy)
                            .font(.title3)
                            .foregroundColor(.white)
                    }
                    
                    Text("Cancel anytime".localized(languages))
                        .fontWeight(.semibold)
                        .font(.callout)
                        .foregroundColor(.white)
                        .opacity(0.7)
                }
                .padding(.top, 35)
                
                VStack(alignment: .leading, spacing: 0){
                    Text("Not sure?".localized(languages))
                        .fontWeight(.black)
                        .font(.title)
                    
                    HStack{
                        Text("Enable trial period".localized(languages))
                            .fontWeight(.semibold)
                            .font(.body)
                        
                        Spacer(minLength: 0)
                        
                        Toggle(isOn: $isPeriodWeek, label: {
                            
                        })
                        .labelsHidden()
                    }
                }
                .foregroundColor(.white)
                .padding(.top, 30)
                .padding(.horizontal, 16)
                
                Button {
                    if isPeriodWeek{
                        subscriptionsData.buyProduct(productType: .weeklySubscription)
                    }else{
                        subscriptionsData.buyProduct(productType: .annualSubscription)
                    }
                } label: {
                    CustomButtonContinue()
                }
                .buttonStyle(.plain)
                .padding(.top, 40)
                
                
                Spacer(minLength: 0)
                
                Button {
                    subscriptionsData.restoreProduct()
                } label: {
                    HStack(spacing: 0){
                        Image("ImageProtected")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: widthIconProtected)
                        
                        Text("Restore Purchases".localized(languages))
                            .fontWeight(.semibold)
                            .font(.title3)
                            .foregroundColor(.white)
                    }
                }
                .buttonStyle(.plain)
                
                
                HStack(spacing: 20){
                    Button {
                        openURL(URL(string: "https://google.com")!)
                    } label: {
                        Text("Terms of use".localized(languages))
                            .fontWeight(.semibold)
                            .font(.subheadline)
                    }
                    .buttonStyle(.plain)
                    
                    
                    Button {
                        openURL(URL(string: "https://google.com")!)
                    } label: {
                        Text("Privacy Policy".localized(languages))
                            .fontWeight(.semibold)
                            .font(.subheadline)
                    }
                    .buttonStyle(.plain)
                    
                    
                }
                .foregroundColor(.white)
                .padding(.top, 18)
            }
        }
        .navigationBarHidden(true)
    }
    
    func getPriceProduct() -> String{
        if isPeriodWeek{
            if let product = subscriptionsData.products.first(where: {$0.id == subscriptionsData.dicProductIds[.weeklySubscription]}){
                return product.displayPrice
            }
        }else{
            if let product = subscriptionsData.products.first(where: {$0.id == subscriptionsData.dicProductIds[.annualSubscription]}){
                return product.displayPrice
            }
        }
        
        return ""
    }
}

struct PremiumView_Previews: PreviewProvider {
    static var previews: some View {
        PremiumView()
    }
}
