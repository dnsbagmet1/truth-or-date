//
//  CustomButtonBack.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI

struct CustomButtonBack: View{
    let sizeIcon = UIScreen.main.bounds.width / 6.57
    
    var body: some View{
        Image("ImageBack")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: sizeIcon)
    }
}

struct CustomButtonBack_Previews: PreviewProvider {
    static var previews: some View {
        CustomButtonBack()
    }
}
