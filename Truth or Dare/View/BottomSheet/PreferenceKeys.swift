//
//  PreferenceKeys.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

struct PresenterPreferenceKey: PreferenceKey {
    static func reduce(value: inout [PreferenceData], nextValue: () -> [PreferenceData]) {
        value.append(contentsOf: nextValue())
    }
    static var defaultValue: [PreferenceData] = []
}

/// Preference Key for the Sheet Content
struct SheetPreferenceKey: PreferenceKey {
    static func reduce(value: inout [PreferenceData], nextValue: () -> [PreferenceData]) {
        value.append(contentsOf: nextValue())
    }
    static var defaultValue: [PreferenceData] = []
}

/// Data Stored in the Preferences
struct PreferenceData: Equatable {
    let bounds: CGRect
}
