//
//  SheetView.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

extension BottomSheet{
    func SheetView() -> some View{
        let drag = dragGesture()
        
        return ZStack{
            if isPresented{
                Group{
                    Color.black.opacity(0.4)
                }
                .edgesIgnoringSafeArea(.vertical)
                .onTapGesture {
                    withAnimation(defaultAnimation){
                        dismissKeyboard()
                        isPresented = false
                    }
                }
            }
            
            Group{
                VStack(spacing: 0){
                    if isHandler {
                        RoundedRectangle(cornerRadius: CGFloat(5.0) / 2.0)
                            .frame(width: 40, height: 5)
                            .foregroundColor(Color.gray)
                            .padding(.vertical)
                    }
                    
                    VStack{
                        content()
                            .padding(.bottom, 5)
                            .background(
                                GeometryReader { proxy in
                                    Color.clear.preference(key: SheetPreferenceKey.self, value: [PreferenceData(bounds: proxy.frame(in: .global))])
                                }
                            )
                    }
                    
                    Spacer(minLength: 0)
                }
                .onPreferenceChange(SheetPreferenceKey.self, perform: { (prefData) in
                    DispatchQueue.main.async {
                        withAnimation(defaultAnimation) {
                            self.sheetContentRect = prefData.first?.bounds ?? .zero
                        }
                    }
                })
                .frame(width: UIScreen.main.bounds.width)
                .background(backgroundColor)
                .clipShape(CustomCorner(corners: [.topLeft, .topRight]))
                
                .offset(y: sheetPosition)
                .gesture(drag)
            }
        }
    }
}



