//
//  BottomSheetDragGesture.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

extension BottomSheet{
    func dragGesture() -> _EndedGesture<_ChangedGesture<DragGesture>> {
        DragGesture(minimumDistance: 20, coordinateSpace: .local)
            .onChanged(onChanged)
            .onEnded(onEnded)
    }
    
    func onChanged(value: DragGesture.Value){
        if !isFixed{
            dismissKeyboard()
            let yOffset = value.translation.height
            let threshold = CGFloat(-50)
            let stiffness = CGFloat(0.3)
            if yOffset > threshold {
                withAnimation(defaultAnimation){
                    dragOffset = value.translation.height
                }
            } else if
                // if above threshold and belove ScreenHeight make it elastic
                -yOffset + sheetContentRect.height <
                    UIScreen.main.bounds.height + self.handlerSectionHeight
            {
                withAnimation(defaultAnimation){
                    let distance = yOffset - threshold
                    let translationHeight = threshold + (distance * stiffness)
                    dragOffset = translationHeight
                }
            }
        }
    }
    
    func onEnded(value: DragGesture.Value){
        if !isFixed{
            /// The drag direction
            let verticalDirection = value.predictedEndLocation.y - value.location.y
            
            // Set the correct anchor point based on the vertical direction of the drag
            if verticalDirection > 1 {
                DispatchQueue.main.async {
                    withAnimation(defaultAnimation) {
                        dragOffset = 0
                        isPresented = false
                    }
                }
            } else if verticalDirection < 0 {
                withAnimation(defaultAnimation) {
                    dragOffset = 0
                    isPresented = true
                }
            } else {
                /// The current sheet position
                let cardTopEdgeLocation = topAnchor + value.translation.height
                
                // Get the closest anchor point based on the current position of the sheet
                let closestPosition: CGFloat
                
                if (cardTopEdgeLocation - topAnchor) < (bottomAnchor - cardTopEdgeLocation) {
                    closestPosition = topAnchor
                } else {
                    closestPosition = bottomAnchor
                }
                
                withAnimation(defaultAnimation) {
                    dragOffset = 0
                    isPresented = (closestPosition == topAnchor)
                }
            }
        }
    }
}
