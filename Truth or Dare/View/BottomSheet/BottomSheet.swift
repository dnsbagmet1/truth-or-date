//
//  BottomSheet.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

struct BottomSheet<Base: View, InnerContent: View>: View {
    @Binding var isPresented: Bool
    
    var isHandler: Bool
    var isFixed: Bool
    var backgroundColor: Color = .white
    
    let base: Base
    let content: () -> InnerContent
    
    /// The rect containing the presenter
    @State var presenterContentRect: CGRect = .zero
    /// The rect containing the sheet content
    @State var sheetContentRect: CGRect = .zero
    /// The offset for keyboard height
    @State var keyboardOffset: CGFloat = 0
    /// The offset for the drag gesture
    @State var dragOffset: CGFloat = 0
    
    /// The height of the handler bar section
     var handlerSectionHeight: CGFloat {
         return isHandler ? 30 : 0
    }
    
    /// The he point for the bottom anchor
    var bottomAnchor: CGFloat {
        return UIScreen.main.bounds.height + 5
    }
    
    var minTopDistance: CGFloat = 110
    
    var topAnchor: CGFloat {
        let topSafeArea = (UIApplication.shared.windows.first?.safeAreaInsets.top ?? 0)
        
        let calculatedTop =
        presenterContentRect.height +
        topSafeArea -
        sheetContentRect.height -
        handlerSectionHeight
        
        guard calculatedTop < minTopDistance else {
            return calculatedTop
        }
        
        return minTopDistance
    }
    
    public var defaultAnimation: Animation = .interpolatingSpring(stiffness: 300.0, damping: 30.0, initialVelocity: 10.0)
    
    var sheetPosition: CGFloat {
        if isPresented {
            // 20.0 = To make sure we dont go under statusbar on screens without safe area inset
            let topInset = UIApplication.shared.windows.first?.safeAreaInsets.top ?? 20.0
            let position = self.topAnchor + self.dragOffset - self.keyboardOffset
            
            if position < topInset {
                return topInset
            }
            
            return position
        } else {
            return self.bottomAnchor - self.dragOffset
        }
    }
    
    var body: some View{
        ZStack{
            base
                .background(
                    GeometryReader { proxy in
                        // Add a tracking on the presenter frame
                        Color.clear.preference(
                            key: PresenterPreferenceKey.self,
                            value: [PreferenceData(bounds: proxy.frame(in: .global))]
                        )
                    }
                )
                .onPreferenceChange(PresenterPreferenceKey.self, perform: { (prefData) in
                    self.presenterContentRect = prefData.first?.bounds ?? .zero
                    DispatchQueue.main.async {
                        
                    }
                })
            
            SheetView()
                .edgesIgnoringSafeArea(.vertical)
        }
    }
}
