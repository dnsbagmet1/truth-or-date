//
//  Extension.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

public extension View{
    func bottomSheet<Content: View>(isPresented: Binding<Bool>, isHandler: Bool = true, isFixed: Bool = false, backgroundColor: Color = .white, @ViewBuilder content: @escaping () -> Content) -> some View {
        BottomSheet(isPresented: isPresented, isHandler: isHandler, isFixed: isFixed, backgroundColor: backgroundColor, base: self, content: content)
    }
}

extension BottomSheet {
    /// Dismiss the keyboard
    func dismissKeyboard() {
        let resign = #selector(UIResponder.resignFirstResponder)
        DispatchQueue.main.async {
            UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
        }
    }
}
