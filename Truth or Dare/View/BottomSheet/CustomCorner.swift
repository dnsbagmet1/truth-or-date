//
//  CustomCorner.swift
//  Lesson
//
//  Created by Денис Багмет on 30.10.2021.
//

import SwiftUI

struct CustomCorner: Shape{
    var corners: UIRectCorner
    
    func path(in rect: CGRect) -> Path{
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: 39, height: 39))
        
        return Path(path.cgPath)
    }
}
