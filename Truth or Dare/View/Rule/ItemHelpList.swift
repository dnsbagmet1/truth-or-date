//
//  ItemHelpList.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct ItemHelpList: View{
    var index: Int = 1
    var title: String = ""
    var body: some View{
        
        let sizeImageIndex = UIScreen.main.bounds.width / 11.02
        
        HStack(alignment: .top, spacing: 13){
            Image("ImageIndex\(index)")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: sizeImageIndex)
            
            Text(title)
            .fontWeight(.semibold)
            .font(.title3)
            .padding(.top, 10)
        }
    }
}

struct ItemHelpList_Previews: PreviewProvider {
    static var previews: some View {
        ItemHelpList()
    }
}
