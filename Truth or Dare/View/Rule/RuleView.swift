//
//  RuleView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import SwiftUI

struct RuleView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @Binding var showHelpView: Bool
    var body: some View {
        VStack(spacing: 0){
            HeaderRuleView(showHelpView: $showHelpView)
            
            HStack{
                Text("Rules".localized(languages))
                    .fontWeight(.heavy)
                    .font(.largeTitle)
                    .padding(.leading, 16)
                
                Spacer(minLength: 0)
                
            }
            
            BlockHelpList()
                .padding(.top, 30)
        }
    }
}

struct RuleView_Previews: PreviewProvider {
    static var previews: some View {
        RuleView(showHelpView: .constant(true))
    }
}
