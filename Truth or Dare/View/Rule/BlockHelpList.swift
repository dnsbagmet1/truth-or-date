//
//  BlockHelpList.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct BlockHelpList: View{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    var body: some View{
        VStack(alignment: .leading, spacing: 30){
            ItemHelpList(index: 1, title: "Choose truth or dare".localized(languages))
            ItemHelpList(index: 2, title: "Earn points by honestly answering questions or completing tasks".localized(languages))
            ItemHelpList(index: 3, title: "If you don’t want to answer a question or complete a task, give up, but you will lose points".localized(languages))
        }
        .padding(.horizontal, 16)
    }
}

struct BlockHelpList_Previews: PreviewProvider {
    static var previews: some View {
        BlockHelpList()
    }
}
