//
//  HeaderRuleView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 26.08.2022.
//

import SwiftUI

struct HeaderRuleView: View{
    @Binding var showHelpView: Bool
    var body: some View{
        
        let sizeIcon = UIScreen.main.bounds.width / 11.78
        
        HStack{
            Spacer(minLength: 0)
            
            Button {
                withAnimation {
                    showHelpView.toggle()
                }
            } label: {
                Image("ImageClose")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: sizeIcon)
            }
            .padding([.trailing, .top], 25)
            .buttonStyle(.plain)
        }
    }
}

struct HeaderRuleView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderRuleView(showHelpView: .constant(true))
    }
}
