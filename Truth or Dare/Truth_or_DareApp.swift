//
//  Truth_or_DareApp.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI
//import SwiftyStoreKit

@main
struct Truth_or_DareApp: App {
    @AppStorage("isPremium") var isPremium: Bool = false
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate{
    @AppStorage("isPremium") var isPremium: Bool = false
    @AppStorage("package") var package: String = ""
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
//            for purchase in purchases {
//                switch purchase.transaction.transactionState {
//                case .purchased, .restored:
//                    if purchase.needsFinishTransaction {
//                        // Deliver content from server, then:
//                        SwiftyStoreKit.finishTransaction(purchase.transaction)
//                    }
//                    // Unlock content
//                case .failed, .purchasing, .deferred:
//                    self.isPremium = false
//                default:
//                    break
//                }
//            }
//        }
//        
//        self.checkSubscription()
        
        return true
    }
    
//    func checkSubscription(){
//        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
//        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
//            switch result {
//            case .success(let receipt):
//                let productId = self.package
//                // Verify the purchase of a Subscription
//                let purchaseResult = SwiftyStoreKit.verifySubscription(
//                    ofType: .autoRenewable, // or .nonRenewing (see below)
//                    productId: productId,
//                    inReceipt: receipt)
//                
//                switch purchaseResult {
//                case .purchased(let expiryDate, let items):
//                    self.isPremium = true
//                    print("\(productId) is valid until \(expiryDate)\n\(items)\n")
//                case .expired(let expiryDate, let items):
//                    self.isPremium = false
//                    print("\(productId) is expired since \(expiryDate)\n\(items)\n")
//                case .notPurchased:
//                    self.isPremium = false
//                    print("The user has never purchased \(productId)")
//                }
//                
//            case .error(let error):
//                self.isPremium = false
//                print("Receipt verification failed: \(error)")
//            }
//        }
//    }
}
