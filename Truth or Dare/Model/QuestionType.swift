//
//  QuestionType.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 27.08.2022.
//

import SwiftUI

enum QuestionType{
    case Truth
    case Dare
}
