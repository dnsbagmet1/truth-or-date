//
//  Language.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 04.09.2022.
//

import SwiftUI

enum Language: String{
    case english = "en"
    case russian = "ru"
    case french = "fr"
    case german = "ge"
    case spanish = "sp"
}

