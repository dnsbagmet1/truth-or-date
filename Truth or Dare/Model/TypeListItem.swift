//
//  TypeListItem.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 28.08.2022.
//

import Foundation

enum TypeListItem {
    case Normal
    case CropUP
    case CropUPAndBottom
    case CropBottom
}
