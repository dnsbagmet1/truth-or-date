//
//  PlayerModel.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import SwiftUI

struct PlayerModel{
    var id = UUID()
    var name: String
    var score: Int
    var countAnswerTruth: Int
    var countAnswerDare: Int
    var countCry: Int
}
