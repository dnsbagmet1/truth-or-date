//
//  PurchaseProductType.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 09.09.2022.
//

import Foundation

enum PurchaseProductType{
    case nonConsumablePurchaseForever
    case annualSubscription
    case weeklySubscription
}
