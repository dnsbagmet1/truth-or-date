//
//  SetsType.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 25.08.2022.
//

import SwiftUI

enum SetsType{
    case WarmUp
    case Contact
    case HenParty
    case BachelorParty
    case Hangout
    case Hardcor
}
