//
//  ContentView.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 23.08.2022.
//

import SwiftUI
import StoreKit
import StoreKitPlus

struct ContentView: View {
    @AppStorage("language")  var languages = LocalizationService.shared.language
    @AppStorage("isFirstRun") var isFirstRun: Bool = true
    @StateObject var subscriptionsData = SubscriptionViewModel()
    @StateObject var gameData = GameViewModel()
    var body: some View {
        ZStack{
            HomeView()
                .alert(isPresented: $subscriptionsData.isError){
                    Alert(title: Text("Erorr"), message: Text(subscriptionsData.errorMsg), dismissButton: .default(Text("Ok")))
                }
            
            if subscriptionsData.isLoading{
                ZStack{
                    Color.white
                        .frame(width: 80, height: 80)
                        .cornerRadius(15)
                    
                    ProgressView()
                        .scaleEffect(1.5)
                }
            }
        }
            .environmentObject(gameData)
            .environmentObject(subscriptionsData)
            .preferredColorScheme(.light)
            .onAppear {
                if isFirstRun{
                    switch Locale.current.languageCode {
                    case "ru":
                        languages = .russian
                    case "fr":
                        languages = .french
                    case "es":
                        languages = .spanish
                    case "de":
                        languages = .german
                    default:
                        languages = .english
                    }
                    
                    isFirstRun = false
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(SubscriptionViewModel())
            .environmentObject(GameViewModel())
    }
}
