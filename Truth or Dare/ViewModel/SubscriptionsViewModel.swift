//
//  SubscriptionStoreKit.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 14.09.2022.
//

import Foundation
import StoreKit
import StoreKitPlus

public enum StoreError: Error {
    case failedVerification
}

class SubscriptionViewModel: ObservableObject{
    let productIds = ["com.appboxworld.truthordare.lifetime",  "com.appboxworld.truthordare.yearly",  "com.appboxworld.truthordare.weekly"]
    let dicProductIds: [PurchaseProductType: String] = [
        .weeklySubscription: "com.appboxworld.truthordare.weekly",
        .annualSubscription: "com.appboxworld.truthordare.yearly",
        .nonConsumablePurchaseForever: "com.appboxworld.truthordare.lifetime"
    ]
    
    @Published var products: [Product] = []
    
    @Published var isLoading: Bool = false
    @Published var isError: Bool = false
    @Published var errorMsg: String = ""
    
    @Published var openPremium: Bool = false
    @Published var isPremium: Bool = false
    
    let context = StoreContext()
    let service: StandardStoreService
    
    init(){
        self.service = StandardStoreService(productIds: self.productIds, context: self.context)
        
        Task{
            do{
                try await service.syncStoreData()
                try await self.getProudcts()
                
                try await self.checkPremium()
            }catch{
                print("Error", error.localizedDescription)
            }
        }
    }
    
    func checkPremium()async throws{
        let purchasedProductIds = context.purchasedProductIds
        DispatchQueue.main.async {
            self.isPremium = !purchasedProductIds.isEmpty
        }
    }
    
    private func getProudcts() async throws{
        let result = try await service.getProducts()
        DispatchQueue.main.async {
            self.products = result
        }
    }
    
    func buyProduct(productType: PurchaseProductType){
        if let product = self.products.first(where: {$0.id == self.dicProductIds[productType]}){
            Task{
                
                DispatchQueue.main.async {
                    self.isLoading = true
                }
                
                do{
                    try await self.purchase(product)
                }catch{
                    self.errorMsg = error.localizedDescription
                    self.isError = true
                }
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
            }
        }
    }
    
    func restoreProduct(){
        Task{
            DispatchQueue.main.async {
                self.isLoading = true
            }
            
            do{
                try await self.restorePurchases()
            }catch{
                self.errorMsg = error.localizedDescription
                self.isError = true
            }
            
            DispatchQueue.main.async {
                self.isLoading = false
            }
        }
    }
    
    private func purchase(_ product: Product) async throws{
        let _ = try await service.purchase(product)
        try await self.checkPremium()
    }
    
    private func restorePurchases() async throws{
        DispatchQueue.main.async {
            self.isLoading = true
        }
        try await service.restorePurchases()
        
        try await self.checkPremium()
        
        DispatchQueue.main.async {
            self.isLoading = false
        }
    }
}
