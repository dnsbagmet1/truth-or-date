//
//  GameViewModel.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import Foundation
import SwiftUI

class GameViewModel: ObservableObject{
    @AppStorage("language")  var languages = LocalizationService.shared.language
    
    @Published var players = [PlayerModel]()
    @Published var currentPlayerIndex: Int = 0
    
    @Published var currentSet: SetsType = .Hangout
    @Published var questionType: QuestionType = .Truth
    
    @Published var currentTask: String = ""
    
    @Published var previewTasks = [String]()
    @Published var previewIndexTasks = [Int]()
    
    @Published var countRounds: Int = 1
    @Published var isFinishedGame: Bool = false
    
    var dicTasks = [QuestionType: [SetsType : [String]]]()
    
    init(){
        
        for _ in 0..<2{
            addNewPlayer()
        }
        
        self.newGame()
    }
    
    func newGame(){
        self.countRounds = 1
        self.currentPlayerIndex = 0
        self.loadTasks()
        
        for index in 0..<players.count{
            players[index].countAnswerTruth = 0
            players[index].countAnswerDare = 0
            players[index].countCry = 0
            
            players[index].score = 0
        }
    }
    
    func addNewPlayer(){
        self.players.append(
            PlayerModel(
                name: "",
                score: 0,
                countAnswerTruth: 0,
                countAnswerDare: 0,
                countCry: 0))
    }
    
    func getCurrentLanguage() -> String{
        switch languages {
        case .english:
            return "en"
        case .russian:
            return "ru"
        case .spanish:
            return ""
        case .german:
            return ""
        case .french:
            return ""
        }
    }
    
    func loadTasks(){
        
        self.dicTasks[.Truth] = [
            .WarmUp: getLinesFromFile(file: "\(getCurrentLanguage())_truth_razminka"),
            .Contact: getLinesFromFile(file: "\(getCurrentLanguage())_truth_znakomstva"),
            .Hangout: getLinesFromFile(file: "\(getCurrentLanguage())_truth_tusovka"),
            .Hardcor: getLinesFromFile(file: "\(getCurrentLanguage())_truth_hardcor"),
            .HenParty: getLinesFromFile(file: "\(getCurrentLanguage())_truth_devichnik"),
            .BachelorParty: getLinesFromFile(file: "\(getCurrentLanguage())_truth_malchishnik")
        ]
        
        self.dicTasks[.Dare] = [
            .WarmUp: getLinesFromFile(file: "\(getCurrentLanguage())_dare_razminka"),
            .Contact: getLinesFromFile(file: "\(getCurrentLanguage())_dare_znakomstva"),
            .Hangout: getLinesFromFile(file: "\(getCurrentLanguage())_dare_tusovka"),
            .Hardcor: getLinesFromFile(file: "\(getCurrentLanguage())_dare_hardcor"),
            .HenParty: getLinesFromFile(file: "\(getCurrentLanguage())_dare_devichnik"),
            .BachelorParty: getLinesFromFile(file: "\(getCurrentLanguage())_dare_malchishnik")
        ]
    }
    
    func getLinesFromFile(file: String) -> [String]{
        var tempTasks = [String]()
        
        if let url = Bundle.main.url(forResource: file, withExtension: "txt"){
            do{
                let file = try String(contentsOf: url, encoding: .utf8)
                let lines = file.split(separator: "\n")
                for line in lines {
                    tempTasks.append(String(line))
                }
            }catch{
                print("[Error]", error.localizedDescription)
            }
        }
        
        return tempTasks
    }
    
    func getTask(){
        if let listTasks = self.dicTasks[self.questionType]?[self.currentSet]{
            let randomInt = Int.random(in: 0..<listTasks.count)
            self.currentTask = listTasks[randomInt]
            
            self.dicTasks[self.questionType]?[self.currentSet]?.remove(at: randomInt)
        }
    }
    
    func getRandomPreviewTask(){
        self.previewTasks.removeAll()
        self.previewIndexTasks.removeAll()
        
        for _ in 0..<3{
            
            let randomIntQuestionType = Int.random(in: 0..<2)
            
            if let listTasks = self.dicTasks[randomIntQuestionType == 0 ? .Truth : .Dare]?[self.currentSet]{
                let randomInt = Int.random(in: 0..<listTasks.count)
                self.previewTasks.append(listTasks[randomInt])
                self.previewIndexTasks.append(randomInt)
            }
        }
    }
    
    func getNameCurrentWin() -> String{
        
        var maxScorePlayer = PlayerModel(name: "", score: 0, countAnswerTruth: 0, countAnswerDare: 0, countCry: 0)
        
        for player in players {
            if player.score > maxScorePlayer.score{
                maxScorePlayer = player
            }
        }
        
        return maxScorePlayer.name
    }
    
    func getNameCurrentPlayer() -> String{
        return self.players[self.currentPlayerIndex].name
    }
    
    func nextPlayer(){
        if self.currentPlayerIndex == self.players.count - 1{
            self.currentPlayerIndex = 0
        }else{
            self.currentPlayerIndex += 1
        }
        
        self.countRounds += 1
    }
    
    func getCountQuestions(type: SetsType) -> Int{
        var count = 0
        
        count = self.dicTasks[.Truth]?[type]?.count ?? 0
        count += self.dicTasks[.Dare]?[type]?.count ?? 0
        
        return count
    }
    
    func addScoreToCurrentPlayer(){
        self.players[self.currentPlayerIndex].score += 1
    }
    
    func checkFinishGame(){
        if self.dicTasks[.Dare]?[self.currentSet]?.isEmpty ?? false{
            self.isFinishedGame = true
        }
        
        if self.dicTasks[.Truth]?[self.currentSet]?.isEmpty ?? false{
            self.isFinishedGame = true
        }
    }
    
    func getImageBySet(set: SetsType) -> String {
        switch set {
        case .WarmUp:
            return "ImageSet\(0)"
        case .Contact:
            return "ImageSet\(1)"
        case .HenParty:
            return "ImageSet\(2)"
        case .BachelorParty:
            return "ImageSet\(3)"
        case .Hangout:
            return "ImageSet\(4)"
        default:
            return "ImageSet\(5)"
        }
    }
    
    func getImageCircleBySet(set: SetsType) -> String{
        switch set {
        case .WarmUp:
            return "ImageCircleSet\(0)"
        case .Contact:
            return "ImageCircleSet\(1)"
        case .HenParty:
            return "ImageCircleSet\(2)"
        case .BachelorParty:
            return "ImageCircleSet\(3)"
        case .Hangout:
            return "ImageCircleSet\(4)"
        case .Hardcor:
            return "ImageCircleSet\(5)"
        }
    }
    
    func getNameBySet(set: SetsType) -> String{
        switch set {
        case .WarmUp:
            return "Warm-up".localized(languages)
        case .Contact:
            return "Acquaintance".localized(languages)
        case .HenParty:
            return "Hen-party".localized(languages)
        case .BachelorParty:
            return "Bachelor party".localized(languages)
        case .Hangout:
            return "Tusovka".localized(languages)
        default:
            return "Hardcore".localized(languages)
        }
    }
}
