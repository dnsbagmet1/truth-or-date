//
//  RoundCornersExtension.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 24.08.2022.
//

import SwiftUI

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
