//
//  Config.swift
//  Truth or Dare
//
//  Created by Денис Багмет on 12.09.2022.
//

import Foundation



let purchaseProducts: [PurchaseProductType : String] = [
    .nonConsumablePurchaseForever : "com.appboxworld.truthordare.lifetime",
    .annualSubscription : "com.appboxworld.truthordare.yearly",
    .weeklySubscription : "com.appboxworld.truthordare.weekly"
]

let sharedSecret = "6367edea87774f439a1ee93602730ee1"
